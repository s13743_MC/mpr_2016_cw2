package pl.edu.pjwstk.mpr.s13743.first_maven_project.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import pl.edu.pjwstk.mpr.s13743.first_maven_project.domain.ClientDetails;

public class ClientDetailsManagerTest {
	
	ClientDetailsManager clientDetailsManager = new ClientDetailsManager();
	
	private final static String NAME_1 = "Stanislaw";
	private final static String SURNAME_1 = "Maczek";
	private final static String LOGIN_1 = "maczek1994";
	
	@Test
	public void checkConnection() {
		assertNotNull(clientDetailsManager.getConnection());
	}
	
	@Test
	public void checkAdding() {
		ClientDetails clientDetails = new ClientDetails(NAME_1, SURNAME_1, LOGIN_1);
		
		clientDetailsManager.clearClientDetails();
		assertEquals(1, clientDetailsManager.addClientDetails(clientDetails));
		
		List<ClientDetails> clientDetailsList = clientDetailsManager.getAllClientDetails();
		ClientDetails clientDetailsRetrieved = clientDetailsList.get(0);
		
		assertEquals(NAME_1, clientDetailsRetrieved.getName());
		assertEquals(SURNAME_1, clientDetailsRetrieved.getSurname());
		assertEquals(LOGIN_1, clientDetailsRetrieved.getLogin());
	}
}