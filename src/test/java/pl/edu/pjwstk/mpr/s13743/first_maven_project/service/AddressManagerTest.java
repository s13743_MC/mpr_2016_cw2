package pl.edu.pjwstk.mpr.s13743.first_maven_project.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import pl.edu.pjwstk.mpr.s13743.first_maven_project.domain.Address;

public class AddressManagerTest {
	
	AddressManager addressManager = new AddressManager();
	
	private final static String STREET_1 = "Arden Street";
	private final static String BUILDING_NUMBER_1 = "16";
	private final static String FLAT_NUMBER_1 = "0";
	private final static String POSTAL_CODE_1 = "EH9 1BP";
	private final static String CITY_1 = "Edinburgh";
	private final static String COUNTRY_1 = "Scotland";
	
	@Test
	public void checkConnection() {
		assertNotNull(addressManager.getConnection());
	}
		
	@Test
	public void checkAdding() {
		Address address = new Address(STREET_1, BUILDING_NUMBER_1, FLAT_NUMBER_1, POSTAL_CODE_1, CITY_1, COUNTRY_1);
		
		addressManager.clearAddress();
		assertEquals(1, addressManager.addAddress(address));
		
		List<Address> addresses = addressManager.getAllAddresses();
		Address addressRetrieved = addresses.get(0);
		
		assertEquals(STREET_1, addressRetrieved.getStreet());
		assertEquals(BUILDING_NUMBER_1, addressRetrieved.getBuildingNumber());
		assertEquals(FLAT_NUMBER_1, addressRetrieved.getFlatNumber());
		assertEquals(POSTAL_CODE_1, addressRetrieved.getPostalCode());
		assertEquals(CITY_1, addressRetrieved.getCity());
		assertEquals(COUNTRY_1, addressRetrieved.getCountry());
	}
}
