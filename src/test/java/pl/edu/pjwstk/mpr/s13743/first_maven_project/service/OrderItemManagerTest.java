package pl.edu.pjwstk.mpr.s13743.first_maven_project.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import pl.edu.pjwstk.mpr.s13743.first_maven_project.domain.OrderItem;

public class OrderItemManagerTest {
	
	OrderItemManager orderItemManager = new OrderItemManager();
	
	private final static String NAME_1 = "M4 Sherman";
	private final static String DESCRIPTION_1 = "It has good mobility and speed, and the armor," +
			" when manually angled, provides moderate shielding. The most popular gun for this tank is the 105 mm M4.";
	private final static double PRICE_1 = 10000.26;
	
	@Test
	public void checkConnection() {
		assertNotNull(orderItemManager.getConnection());
	}
	
	@Test
	public void checkAdding() {
		OrderItem orderItem = new OrderItem(NAME_1, DESCRIPTION_1, PRICE_1);
		
		orderItemManager.clearOrderItem();
		assertEquals(1, orderItemManager.addOrderItem(orderItem));
		
		List<OrderItem> orderItems = orderItemManager.getAllOrderItems();
		OrderItem orderItemRetrieved = orderItems.get(0);
		
		assertEquals(NAME_1, orderItemRetrieved.getName());
		assertEquals(DESCRIPTION_1, orderItemRetrieved.getDescription());
		assertEquals(PRICE_1, orderItemRetrieved.getPrice(), 0.01);
	}
}
